//WAP to reverse each elements in an array

import java.io.*;
class Array{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new  BufferedReader(new InputStreamReader(System.in));
	        int size = Integer.parseInt(br.readLine());
	        int arr[] = new int[size];
	        
	        System.out.println("Enter Array Elements:");
	        for(int i=0;i<arr.length;i++){
		        arr[i]=Integer.parseInt(br.readLine());
		}
	        for(int i=0;i<arr.length;i++){
			int N=arr[i];
			int rev=0;
			while(N!=0){
				int rem=N%10;
				rev=rev*10+rem;
				N=N/10;
			}
			System.out.println(rev);
		}
	}
}

	        
		        	
