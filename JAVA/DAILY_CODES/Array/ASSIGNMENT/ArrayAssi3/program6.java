//WAP to find a palindrome number from an array and return its index

import java.io.*;
class Array{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new  BufferedReader(new InputStreamReader(System.in));
	        int size = Integer.parseInt(br.readLine());
	        int arr[] = new int[size];
	        
	        System.out.println("Enter Array Elements:");
	        for(int i=0;i<arr.length;i++){
		        arr[i]=Integer.parseInt(br.readLine());
		}
	        for(int i=0;i<arr.length;i++){
		        int N=arr[i];
			int rev=0;
			int temp=N;
			while(N!=0){
		               int rem=N%10;
			       rev=rev*10+rem;
			       N=N/10;
				}
			if(temp==rev){
				System.out.println("palindrome no "+arr[i]+" found at index: "+i);
			}
		}
	}
}





