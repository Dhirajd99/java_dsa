class RCB{
	public static void main(String[]args){
		int arr[]={10,20,30,40,50};
		char arr1[]={'A','B','C'};
		float arr2[]={10.5f,20.5f,30.5f};
		boolean arr3[]={true,false,true};
		
		//printing integer arr[]
		System.out.println(arr[0]);
		System.out.println(arr[1]);
		System.out.println(arr[2]);
		System.out.println(arr[3]);
		System.out.println(arr[4]);

                //printing char arr1[]

		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1[2]);
    
                //printing float arr2[]

		System.out.println(arr2[0]);
		System.out.println(arr2[1]);
		System.out.println(arr2[2]);

                //printing boolean arr3[]

		System.out.println(arr3[0]);
		System.out.println(arr3[1]);
		System.out.println(arr3[2]);
	}
}

