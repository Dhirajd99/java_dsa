import java.util.*;
class Add{
        static void add(int a,int b){         //parameters
		int ans1 = a+b;
		System.out.println("ans= " + ans1);
	}
	static void sub(int a,int b){
		int ans2 = a-b;
		System.out.println("ans= "+ ans2);
	}
	static void mul(int a,int b){
		int ans3 = a*b;
		System.out.println("ans= "+ ans3);
	}
	static void div(int a,int b){
		if(b==0){
			System.out.println("infinity");
		}else{
			int ans4 = a/b;
			System.out.println("ans= "+ ans4);
		}
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("enter int values");
		int a = sc.nextInt();
		int b = sc.nextInt();

		add(a,b);     		//arguments
		sub(a,b);
		mul(a,b);
		div(a,b);
	}
}


