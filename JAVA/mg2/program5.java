/* 0
   1 1
   2 3 5
   8 13 21 34  */

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter no of rows");
		int row=Integer.parseInt(br.readLine());

		int n1=0;
		int n2=1;
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=i;j++){
					System.out.print(n1+" ");
					int temp=n2;
					n2=n1+n2;
					n1=temp;
			}
			System.out.println();
		}
	}
}

